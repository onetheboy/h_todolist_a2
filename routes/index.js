const express = require("express");
const router = express.Router();

const db = require("../db");

router.get("/", function (req, res, next) {
  db((err, connection) => {
    connection.query("SELECT * FROM TODO", (err, rows) => {
      connection.release(); // 연결세션 반환.
      if (err) {
        throw err;
      }
      return res.render("index", { todos: rows });
    }); // 결과는 rows에 담아 전송
  });
});

router.post("/add", function (req, res, next) {
  db((err, connection) => {
    connection.query(
      "INSERT INTO TODO (task)VALUES(?)",
      [req.body.task],
      (err, rows) => {
        connection.release(); // 연결세션 반환.
        if (err) {
          throw err;
        }
        return res.redirect("/");
      }
    );
  });
});

router.put("/moveTaskDone", function (req, res, next) {
  console.log("move");
  const { name, id } = req.body;

  var status = 0;
  if (name === "todo") {
    status = 1;
  } else {
    status = 0;
  }

  db((err, connection) => {
    connection.query(
      "UPDATE TODO SET status = ? WHERE id = ?",
      [status, req.body.id],
      (err, rows) => {
        connection.release(); // 연결세션 반환.
        if (err) {
          throw err;
        }
        return res.json({ data: status }); // 결과는 rows에 담아 전송
      }
    );
  });
});

module.exports = router;
